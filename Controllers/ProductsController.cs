﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DeliVeggie.Models.Enums;
using DeliVeggie.Models.Requests;
using DeliVeggie.Models.Responses;
using DeliVeggie.RabbitMq;
using Microsoft.AspNetCore.Mvc;

namespace DeliVeggie.ApiGateway.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IPublisher _publisher;
        public ProductsController(IPublisher publisher)
        {
            _publisher = publisher;
        }
        [HttpGet]
        [Route("GetProducts")]
        public IActionResult GetAll()
        {
            try
            {
                if (!(_publisher.Publish(new RequestMessage {RequestType = RequestType.ProductsRequest}) is
                    ResponseMessage<ICollection<ProductsResponse>> response))
                {
                    return NotFound();
                }
                return Ok(response.Payload);
            }
            catch (Exception e)
            {
                return Problem(e.ToString());
            }
        }
        
        [HttpGet]
        [Route("GetProductDetails")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                if (!(_publisher.Publish(new RequestMessage {RequestType = RequestType.ProductDetailsRequest, Id = id}) is
                    ResponseMessage<ProductDetailsResponse> response))
                {
                    return NotFound();
                }
                return Ok(response.Payload);
            }
            catch (Exception e)
            {
                return Problem(e.ToString());
            }
        }
    }
}